<?php
// you can write to stdout for debugging purposes, e.g.
// print "this is a debug message\n";

const N_MAX = 2000000;
const OPEN = [
    ']' => '[',
    ')' =>'(' ,
    '}' => '{',
];
const CLOSE = [
    '[' => ']',
    '(' => ')',
    '{' => '}',
];

function solution($S) {
    if ('' === $S) {
        return 1;
    }
    
    $n = strlen($S);
    if (N_MAX < $n) {
        return 0;
    }
    
    if (0 !== $n % 2) {
        return 0;
    }
    
    $stack = new SplStack();
    for ($i = 0; $i < $n; ++$i) {
        if (in_array($S[$i], OPEN)) {
            $stack->push($S[$i]);
        } elseif (in_array($S[$i], CLOSE)) {
            if ($stack->isEmpty()) {
                return 0;
            }
            if ($stack->pop() !== OPEN[$S[$i]]) {
                return 0;
            }
        }
    }
    
    if ($stack->isEmpty()) {
        return 1;
    }
    
    return 0;
}
